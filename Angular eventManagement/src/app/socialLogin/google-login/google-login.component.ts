import { Component, EventEmitter, Output } from '@angular/core';
import { SocialAuthService ,SocialUser} from "@abacritt/angularx-social-login";
import { GoogleLoginProvider } from "@abacritt/angularx-social-login";
import { OrganiseService } from 'src/app/organise.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-google-login',
  templateUrl: './google-login.component.html',
  styleUrls: ['./google-login.component.css']
})
export class GoogleLoginComponentCustom {

  socialUser!: SocialUser;
  isLoggedin?: boolean;
  thumbnail: any;


  @Output(`userLoggedIn`) userLoggedIn: EventEmitter<any> = new EventEmitter();

  constructor(private authService: SocialAuthService, private service: OrganiseService,private router: Router) { }

  ngOnInit() {
  
    this.authService.authState.subscribe((user) => {
      console.log('user ',user)
      this.socialUser = user;
      this.isLoggedin = user != null;
      if(user.idToken){
        this.service.setUserLoggedIn(user.email);
        this.router.navigate(['event']);
      }
      console.log(this.socialUser);
    });
  }

  loginWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }
  logOut(): void {
    this.authService.signOut();
  }
  refreshToken(): void {
    console.log(GoogleLoginProvider.PROVIDER_ID)
    this.authService.refreshAuthToken(GoogleLoginProvider.PROVIDER_ID);
  }

}
