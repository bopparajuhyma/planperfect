import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OrganiseService } from '../organise.service';
@Component({
  selector: 'app-bwed',
  templateUrl: './bwed.component.html',
  styleUrls: ['./bwed.component.css']
})
export class BwedComponent implements OnInit {

  catering : any;
  decoration:any;
  photography:any;
  makeup:any;
  music:any;



  constructor(private router: Router, private service: OrganiseService) {

  }
  ngOnInit() {

     this.catering= [
      { name:"Food",   price:150, description:"Food is limited", imgsrc:"assets/Images/img1.jpg"},
      { name:"Food", price:250, description:"Food is unlimited with few sweets", imgsrc:"assets/Images/img2.jpg"},
      { name:"Food",  price:300, description:"Food is unlimited", imgsrc:"assets/Images/img3.jpg"}];
    







      
    }  

      addToCart(product: any) {
        this.service.addToCart(product);
      }
      
}