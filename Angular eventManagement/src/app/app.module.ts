import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HeaderComponent } from './header/header.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { EventComponent } from './event/event.component';
import { AboutComponent } from './about/about.component';
import { HomeComponent } from './home/home.component';
import { BlogsComponent } from './blogs/blogs.component';
import { OurservicesComponent } from './ourservices/ourservices.component';
import { MeComponent } from './me/me.component';
import { LogoutComponent } from './logout/logout.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { WeddingosComponent } from './weddingos/weddingos.component';
import { CorporateComponent } from './corporate/corporate.component';
import { PartiesComponent } from './parties/parties.component';
import { SocialLoginModule, SocialAuthServiceConfig } from '@abacritt/angularx-social-login';
import {
  GoogleLoginProvider,
} from '@abacritt/angularx-social-login';
import {  GoogleSigninButtonModule } from '@abacritt/angularx-social-login';

import { PartiesevComponent } from './partiesev/partiesev.component';
import { CorporateevComponent } from './corporateev/corporateev.component';
import { PartiesservComponent } from './partiesserv/partiesserv.component';
import { WedeventComponent } from './wedevent/wedevent.component';
import { BwedComponent } from './bwed/bwed.component';
import { ForgetPassComponent } from './forgotpassword/forgotpassword.component';
import { BookComponent } from './book/book.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ShowbookedComponent } from './showbooked/showbooked.component';
import { GoogleLoginComponentCustom } from './socialLogin/google-login/google-login.component'


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    EventComponent,
    AboutComponent,
    HomeComponent,
    BlogsComponent,
    OurservicesComponent,
    MeComponent,
    LogoutComponent,
    WeddingosComponent,
    CorporateComponent,
    PartiesComponent,
    PartiesevComponent,
    CorporateevComponent,
    PartiesservComponent,
    WedeventComponent,
    BwedComponent,
    ForgetPassComponent,
   BookComponent,
   ResetPasswordComponent,
   ShowbookedComponent,
   GoogleLoginComponentCustom,
   HeaderComponent,
   
  //  GoogleLoginComponent,
  //  FilterByPrcNamePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),  // Add BrowserAnimationsModule to enable Angular Material animations
    SocialLoginModule,
    ReactiveFormsModule,
    GoogleSigninButtonModule
  ],
  providers: [
    // FilterByPrcNamePipe
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              '479005336409-mqit3qjh3iu93j2i9h8u3ugpdqgdj2ue.apps.googleusercontent.com'
            )
          }
        ],
        onError: (err) => {
          console.error(err);
        }
      } as SocialAuthServiceConfig,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
// 309111779409-5b6oesjbmtfudlbma59s35729kff4gf0.apps.googleusercontent.com